<?php
spl_autoload_register(function ($class) {
    include_once __DIR__ . DIRECTORY_SEPARATOR . str_replace('\\', DIRECTORY_SEPARATOR, $class) . '.php';
});
use Seal\Request;

global $course;

echo Request::postJson(
    'https://omscentral-api-production.herokuapp.com/graphql',
    [
        'query' => '
            query reviews(
              $order_by_desc: [String!]! = ["created"],
              $offset: Int = 0, $limit: Int = 100,
              $course_id: String! = "",
              $author_ids: [String!]! = [],
              $semester_ids: [String!]! = [],
              $query: String = ""
            ) {
              reviews(
                order_by_desc: $order_by_desc
                offset: $offset
                limit: $limit
                course_ids: [$course_id]
                author_ids: $author_ids
                semester_ids: $semester_ids
                query: $query
              ) {
                id
                author {
                  id
                  __typename
                }
                course {
                  id
                  name
                  link
                  __typename
                }
                semester {
                  id
                  name
                  season
                  __typename
                }
                difficulty
                rating
                workload
                body
                created
                updated
                __typename
              }
            }
        ',
        'variables' => [
            'course_id' => str_replace(' ', '-', $course),
            'semester_id' => $_GET['semester_id']
        ],
    ],
);