<?php

namespace Seal;

use DOMNodeList;
use ArrayIterator;

abstract class DOMUtils {
    public static function getDOMNodeListIterator(DOMNodeList $nodes): ArrayIterator {
        return new ArrayIterator(self::getDOMNodeListArray($nodes));
    }

    public static function getDOMNodeListArray(DOMNodeList $nodes): array {
        $arr = [];
        $i = 0;
        while ($i < $nodes->count()) {
            $arr[] = $nodes->item($i);
            $i++;
        }
        return $arr;
    }
}
