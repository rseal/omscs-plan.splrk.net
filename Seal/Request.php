<?php
namespace Seal;

use DOMDocument;

class Request {

    public static function postJson(String $url, array $data, array $headers = []): array {
        $sess = curl_init();

        $content = json_encode($data);
        array_push($headers, [
            'Content-Type: application/json',
            'Accept: application/json',
            'Content-Length: ' . strlen($content),
        ]);

        curl_setopt_array($sess, [
            CURLOPT_URL => $url,
            CURLOPT_HEADER => false,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HTTPHEADER => $headers,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $content,
        ]);

        $result = curl_exec($sess);
        curl_close($sess);

        return json_decode($result);
    }

    public static function getText(String $url, array $headers = []): String {
        $sess = curl_init();

        curl_setopt_array($sess, [
            CURLOPT_URL => $url,
            CURLOPT_HEADER => false,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HTTPHEADER => $headers
        ]);

        $result = curl_exec($sess);
        curl_close($sess);

        return $result;
    }

    public static function getDOM(String $url, array $headers = []): DOMDocument {
        $result = self::getText($url, $headers);

        $doc = new DOMDocument();

        libxml_use_internal_errors(true);
        $doc->loadHTML($result, LIBXML_NOWARNING && LIBXML_NOERROR);
        libxml_clear_errors();

        return $doc;
    }
}
