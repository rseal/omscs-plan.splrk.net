<?php
spl_autoload_register(function ($class) {
    include_once __DIR__ . DIRECTORY_SEPARATOR . str_replace('\\', DIRECTORY_SEPARATOR, $class) . '.php';
});

use GATech\Course;
use GATech\CourseCollection;
use GATech\CourseDetails;
use GATech\Specialization;
use Seal\Request;

$doc = Request::getDOM('https://omscs.gatech.edu/current-courses');
$xpath = new DOMXPath($doc);
$coursesHTML = $xpath->query(".//h3[text() = 'Current & Ongoing OMS Courses']/following-sibling::ul/li");

$courses = new CourseCollection();
foreach($coursesHTML as $listItem) {
    $course = Course::fromListItem($listItem);
    if ($course->isValid) {
        if ($course->link) {
            $doc = Request::getDOM($course->link);
            $course->details = CourseDetails::fromDOM($doc);
        }
        $courses[] = $course;
    }
}

$specializationsDoc = Request::getDOM(Specialization::SPECIALIZATIONS_URL);
$xpath = new DOMXPath($specializationsDoc);
$specializationsList = $xpath->query(".//h4[text() = 'Specializations Currently Offered:']/following-sibling::ul/li");

$specializations = [];
foreach($specializationsList as $listItem) {
    $link = $xpath->query('./a', $listItem)[0];
    $url = $link->attributes->getNamedItem('href')->value;

    $specializationDoc = Request::getDOM($url);
    $specializations[] = Specialization::fromDOM($specializationDoc, $courses, $link->textContent);
}

echo json_encode([
    'courses' => $courses,
    'specializations' => $specializations,
], JSON_PRETTY_PRINT);
