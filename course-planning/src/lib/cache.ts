
const MILLISECONDS_IN_A_DAY = 1000 * 60 * 60 * 24;

export async function cachedFetch<T = Record<string, unknown>>(url: string, options: RequestInit = {}, timeout = MILLISECONDS_IN_A_DAY): Promise<T> {
    const key = btoa(url + '%%%' + (options.body || ''));

    const cache = localStorage.getItem(key);
    const now = Date.now();

    if (cache) {
        const cachedResult = JSON.parse(cache);
        if ((now - cachedResult.time) < timeout) {
            return cachedResult.data;
        }
    }

    const data = await fetch(url, options).then(res => res.json());

    localStorage.setItem(key, JSON.stringify({ time: now, data }));
    return data;
}