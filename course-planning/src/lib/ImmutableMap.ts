import { writable, Writable } from 'svelte/store';
import {generateUniqueId} from "./uniqueId";

export interface WritableMap<V> extends Writable<{ [k: string]: V }> {
    removeItem: (key: string) => void,
    updateItem: (key: string, value: Partial<V>) => void
}

export interface WritableMapWithId<V extends { id: number }> extends WritableMap<V> {
    addItem: (value: Omit<V, 'id'>) => void;
}

export function writableMap<V>(store: Writable<{ [key: string]: V }>, createDefault = (key: string) => ({} as Partial<V>)): WritableMap<V> {
    return {
        ...store,
        removeItem: (key: string) => store.update(prev => {
            const {
                [key]: _,
                ...rest
            } = prev;
            return rest;
        }),
        updateItem: (key: string, value) => store.update(prev => {
            return {
                ...prev,
                [key]: {
                    ...createDefault(key),
                    ...(prev[key] || {}) as V,
                    ...value
                },
            }
        }),
    };

}

export type WithId<V> = V & { id: number };

export function writableMapWithId<V extends { id: number }>(store: Writable<{ [key: string]: V }>): WritableMapWithId<WithId<V>> {
    const mapStore = writableMap(store);
    return {
        ...mapStore,
        addItem: (value: Omit<V, 'id'>) => store.update(currentValues => {
            const newId = generateUniqueId(currentValues);
            return {
                ...currentValues,
                [newId]: {
                    ...value,
                    id: newId,
                }
            };
        }),
    }
}

function reduceItems<K extends string | number, V>(mapper: (key: K, value: V) => [K, V]) {
    return (finalValues, [key, value]) => {
        const [nextKey, nextValue] = mapper(key, value);
        return {
            ...finalValues,
            [nextKey]: nextValue,
        };
    }
}

export function writableFromLocalStorage<V>(
    localStorageKey: string,
    onReadItem: (k: string, v: any) => [string, V] = (k, v) => [k, v],
    onSaveItem: (k: string, v: any) => [string, V] = (k, v) => [k, v],
): Writable<V> {
    let initialValues = (JSON.parse(localStorage.getItem(localStorageKey) || '{}'))
    initialValues = Object.entries(initialValues).reduce(reduceItems(onReadItem), {});

   const store = writable(initialValues);

   store.subscribe(newMap => {
       localStorage.setItem(
           localStorageKey,
           JSON.stringify(
               Object.entries(newMap).reduce(reduceItems(onSaveItem), {})
           )
       );
   });

   return store;
}
