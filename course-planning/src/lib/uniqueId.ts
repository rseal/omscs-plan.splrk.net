
type HasId = { [key: string]: any } & { id: number };
type IDCollection = number[] | { [key: string]: HasId };
type IDMap = Map<number, any>;

export function generateUniqueId(idCollection: IDCollection): number {
    const currentIds: number[] = Array.isArray(idCollection) ? idCollection : Object.values(idCollection).map(({ id }) => id);
    let id: number;
    do {
        id = Math.floor(Math.random() * Number.MAX_SAFE_INTEGER);
    } while(id in currentIds);
    return id;
}

export function generateUniqueIdForMap(idMap: IDMap): number {
    let id: number;
    do {
        id = Math.floor(Math.random() * Number.MAX_SAFE_INTEGER);
    } while(idMap.has(id));
    return id;
}
