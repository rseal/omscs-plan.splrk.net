import { writable, readable } from 'svelte/store';
import { cachedFetch } from "./cache";

/**
 * Details about a course scraped form the OMSCS Website.
 */
export interface CourseDetails {

    /**
     * HTML String of the course details
     */
    overview: string;
    
    /**
     * HTML String of what the course aims to teach studends
     */
    courseGoals: string;
    
    /**
     * Array of URLS to previous syllabi from past semesters
     */
    sampleSyllabus: string[];

    /**
     * URL to the course videos
     */
    videos: string;

    /**
     * HTML string of the suggested course prerequisites
     */
    prerequisites: string;
};

export interface Course {
    /**
     * Is the course one of teh Foundational Courses that must be passed in the first year of OMSCS.
     */
    isFoundational: boolean;

    /**
     * Full name of the course
     */
    name: string;
    
    /**
     * Any Name the Course previously went By.  May be an empty string.
     */
    formerly: string;

    /**
     * The Course Code in the form of "{DEPT} {NUMBER}""
     */
    code: string;

    /**
     * Abbreviation of the department the course is offered in
     */
    dept: string;
    
    /**
     * Georgia Tech's Unique number that identifies the course.
     */
    number: string;
    
    /**
     * The Link to course information page on the OMSCS website.
     */
    link: string;
    
    /**
     * Details scraped from the OMSCS website
     */
    details: CourseDetails;
    
    /**
     * Flag used in scraping to filter out false positives.  Should always be true;
     */
    isValid: true;

    /**
     * A Map of specializations and if the course satisfies a core or an elective requirement
     * of that specialization.
     */
    requirements?: {
        [key: string]: string;
    }
};

export type CourseMap = { [key: string]: Course };

export type CourseRequirements = {
    /**
     * The number of classes to satisfy the requirement
     */
    minimumNeeded: number;

    /**
     * The Courses that can satisfy the requirement
     */
    courseListing: CourseMap;
};

export type Specialization = {
    /**
     * Name of the specialization.
     */
    name: string;

    /**
     * Courses that are are required as part of the core requirement
     */
    coreSelectionPrimary: CourseRequirements;

    /**
     * The secondary set of courses that satisfy the core requirement
     */
    coreSelectionSecondary: CourseRequirements;

    /**
     * The electives that can satisfy the specialization
     */
    electives: CourseRequirements;
};

const CourseSpecializationType = {
    Core: 'C',
    Elective: 'E',
    None: ''
};

function specializationType(specializations, specializationName, code) {
    let type = CourseSpecializationType.None;
    const specialization = specializations.find(({ name }) => name === specializationName);

    if (specialization) {
        if (
            specialization.coreSelectionPrimary.courseListing[code]
            || specialization.coreSelectionSecondary.courseListing[code]) {
            type = CourseSpecializationType.Core;
        } else if (
            specialization.electives.courseListing[code]
        ) {
            type = CourseSpecializationType.Elective;
        }
    }
    return type;
}

export const courses = readable(
    cachedFetch<{ courses: CourseMap, specializations: Specialization[] }>('/courses.json', { cache: 'no-cache' })
        .then(({ courses, specializations }) => {
            for (let code in courses) {
                courses[code].requirements = {
                    ML: specializationType(specializations, 'Machine Learning', code),
                    CS: specializationType(specializations, 'Computing Systems', code),
                    II: specializationType(specializations, 'Interactive Intelligence', code),
                    CR: specializationType(specializations, 'Computational Perception & Robotics', code),
                }
            }

            return { courses, specializations };
        }),
    () => { }
);

export interface Schedule {
    capacity: number;
    taken: number;
    remaining: number;
    waitListCapacity: number;
    waitListTaken: number;
    waitListRemaining: number;
}

export type ScheduleMap = { [key: string]: Schedule };
export type SemesterMap = { [key: string]: ScheduleMap };

const schedulesRaw = writable<SemesterMap>(JSON.parse(localStorage.getItem('schedules') || '{}'));

export const schedules = {
    subscribe: schedulesRaw.subscribe,
    updateSemester: (semester: string, data: string) => {
        const matches = Array.from(data.matchAll(/\s*([A-Z]+)\s+(\d+)\s+(O\d+)\s+[\w\W]+?TBA\s+(\d+)\s+(\d+)\s+(\d+)\s+(\d+)\s+(\d+)\s+(\d+)/mg));
        return schedulesRaw.update(prevSchedule => ({
            ...prevSchedule,
            [semester]: matches.reduce((schedule, match) => {
                return {
                    ...schedule,
                    [`${match[1]} ${match[2]}${match[3] !== 'O01' ? ' ' + match[3] : ''}`]: {
                        capacity: Number.parseInt(match[4], 10),
                        taken: Number.parseInt(match[5], 10),
                        remaining: Number.parseInt(match[6], 10),
                        wailListCapacity: Number.parseInt(match[7], 10),
                        waitListTaken: Number.parseInt(match[8], 10),
                        waitListRemaining: Number.parseInt(match[9], 10),
                    }
                }
            }, {})
        }))
    }
}

schedulesRaw.subscribe(value => localStorage.setItem('schedules', JSON.stringify(value)));

export function semesterFriendlyName(semester) {
    const match = semester.match(/(\d{4})-(\d)/);
    if (match) {
        const [, year, number] = match;
        let name = number === '1' ? 'Spring' : number === '2' ? 'Summer' : 'Fall';
        return `${name} ${year}`;
    }
    return semester;
}


