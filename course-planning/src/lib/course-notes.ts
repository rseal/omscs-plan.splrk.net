import { get, writable } from 'svelte/store';
import {writableMap, writableFromLocalStorage, WritableMap, writableMapWithId} from "./ImmutableMap";

export enum InterestLevel {
    Low = 1,
    Medium,
    High
};

export interface Aspect {
    id: number;
    description: string;
}

export type AspectCollection = { [key: number]: Aspect };

export enum ToDoStatus {
    Incomplete,
    InProgress,
    Complete,
};

export interface ToDo {
    id: number;
    description: string;
    status: ToDoStatus
    isCourse: boolean;
}

export type ToDoCollection = { [key: number]: ToDo };

export interface CourseNotes {
    code: string;
    interestLevel:  InterestLevel;
    pros: WritableMap<Aspect>;
    cons: WritableMap<Aspect>;
    goals: number[];
    toDos: WritableMap<ToDo>;
}

export type CourseNotesCollection = { [key: string]: CourseNotes }

export interface CourseNotesMap extends WritableMap<{ [key: string]: CourseNotes }> {
    newItem
}

export const courseNotes = writableMap(writableFromLocalStorage<{ [key: string]: CourseNotes }>(
    'course-notes',
    (courseCode, notes) => {
        /*let pros = notes.pros || {};
        const prosStore = writableMapWithId(pros);
        prosStore.subscribe()*/

        return [
            courseCode,
            {
                ...notes,
                pros: writableMapWithId<Aspect>(writable(notes.pros || {})),
                cons: writableMapWithId<Aspect>(writable(notes.cons || {})),
                toDos: writableMapWithId<ToDo>(writable(notes.toDos || {})),
            }
        ]
    },
    (courseCode, notes) => {
        return [
            courseCode,
            {
                ...notes,
                pros: get(notes.pros),
                cons: get(notes.cons),
                toDos: get(notes.toDos),
            }
        ]
    }
), (courseCode) => ({
    courseCode,
    interestLevel: 0,
    goals: [],
    pros: writableMapWithId<Aspect>(writable({})),
    cons: writableMapWithId<Aspect>(writable({})),
    toDos: writableMapWithId<ToDo>(writable({})),
}));
