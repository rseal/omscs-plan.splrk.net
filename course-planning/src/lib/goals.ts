import { courseNotes } from './course-notes';
import {writableFromLocalStorage, writableMap, writableMapWithId} from "./ImmutableMap";

interface Goal {
    id: number;
    description: string;
    courses?: string[];
}
type GoalCollection = { [key: number]: Goal };

export const goals = writableMapWithId(writableFromLocalStorage<GoalCollection>('goals'));

courseNotes.subscribe(($courseNotes) => {
    goals.update($goals => {
        return Object.keys($goals).reduce((newGoals, id) => {
            const currentGoal = $goals[id];
            const courses = Object.keys($courseNotes).filter(code => $courseNotes[code]?.goals?.indexOf(currentGoal.id) > -1);
            return {
                ...newGoals,
                [id]: {
                    ...currentGoal,
                    courses
                },
            };
        }, {});
    });
});


