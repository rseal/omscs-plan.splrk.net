import {writableFromLocalStorage, WithId, writableMapWithId} from "./ImmutableMap";
import {CourseNotes} from "./course-notes";

export interface CoursePlan {
    course: string;
    semester: string;
}

export interface DegreePlan {
    score: number;
    courses: CoursePlan[];
}

export const degreePlans = writableMapWithId<WithId<DegreePlan>>(
    writableFromLocalStorage<{ [key: string]: WithId<DegreePlan> }>('degree-plans')
);
