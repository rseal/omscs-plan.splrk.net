import { readable } from 'svelte/store';
import { cachedFetch } from "./cache";

export const semesters = readable(cachedFetch('/semesters.json'));
