<?php
spl_autoload_register(function ($class) {
    include_once __DIR__ . DIRECTORY_SEPARATOR . str_replace('\\', DIRECTORY_SEPARATOR, $class) . '.php';
});

header('ContentType: application/json');
header('Access-Control-Allow-Origin: *');

$matches = [];
if (preg_match('/\/course\/([^\/]+)$/', $_SERVER['REQUEST_URI'], $matches)) {
    $course = $matches[1];
    include __DIR__ . '/course.php';
} else if (preg_match('/\/course\/([^\/]+)\/reviews$/', $_SERVER['REQUEST_URI'], $matches)) {
    $course = $matches[1];
    include __DIR__ . '/reviews.php';
} else if (preg_match('/\/semesters$/', $_SERVER['REQUEST_URI'], $matches)) {
    include __DIR__ . '/semesters.php';
} else {
    include __DIR__. '/courses.php';
}