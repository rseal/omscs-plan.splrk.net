<?php
spl_autoload_register(function ($class) {
    include_once __DIR__ . DIRECTORY_SEPARATOR . str_replace('\\', DIRECTORY_SEPARATOR, $class) . '.php';
});
use GATech\CourseDetails;
use Seal\Request;

$course = $argv[1];
echo $course;

$doc = Request::getDOM('https://omscs.gatech.edu/' . $course);
$details = CourseDetails::fromDOM($doc);

echo json_encode($details);
