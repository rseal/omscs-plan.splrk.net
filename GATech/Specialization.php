<?php
namespace GATech;

use DOMDocument;
use DOMNode;
use DOMXPath;
use Iterator;
use Seal\DOMUtils;

class Specialization
{
    public String $name;
    public CourseCollection $coreSelectionPrimary;
    public CourseCollection $coreSelectionSecondary;
    public CourseCollection $electives;

    const SPECIALIZATIONS_URL = 'https://omscs.gatech.edu/program-info/specializations';

    public function __construct(String $name) {
        $this->name = $name;
    }

    private function findNextCoursesFromDOM(DOMXPath $xpath, Iterator $it, CourseCollection $cachedCourses): CourseCollection {
        $coreCourses = new CourseCollection();
        $coreCourses->minimumNeeded = 1;

        $it->next();
        while ($it->valid() && $it->current()->nodeName !== 'ul') {
            $match = null;
            if (preg_match('/\\((\d+)\\).*(of|from):/i', $it->current()->textContent, $match)) {
                $coreCourses->minimumNeeded = intval($match[1]);
            }
            $it->next();
        }

        while ($it->valid() && !preg_match('/\\((\d+)\\).*(of|from):/i', $it->current()->textContent)) {
            if ($it->current()->nodeName === 'ul') {
                $coursesList = $xpath->query('.//li//strong', $it->current());
                foreach($coursesList as $listItem) {
                    $course = Course::fromListItem($listItem);
                    if (isset($cachedCourses[$course->code])) {
                        $coreCourses[] = $cachedCourses[$course->code];
                    } else {
                        $coreCourses[] = $course;
                    }
                }
            }
            $it->next();
        }

        return $coreCourses;
    }

    private function addCoreCoursesFromDOM(DOMXPath $xpath, DOMNode $mainBlock, CourseCollection $cachedCourses) {
        $nodes = $xpath->query("//p[*[contains(text(), 'Core Courses')]]/following-sibling::node()", $mainBlock);

        if (count($nodes) > 0) {
            $it = DOMUtils::getDOMNodeListIterator($nodes);
            $this->coreSelectionPrimary = $this->findNextCoursesFromDOM($xpath, $it, $cachedCourses);
            $this->coreSelectionSecondary = $this->findNextCoursesFromDOM($xpath, $it, $cachedCourses);
        }
    }

    private function addElectiveCoursesFromDOM(DOMXPath $xpath, DOMNode $mainBlock, CourseCollection $cachedCourses) {
        $nodes = $xpath->query(".//p[*[contains(text(), 'Electives')]]/following-sibling::node()", $mainBlock);

        if ($nodes) {
            $this->electives = $this->findNextCoursesFromDOM($xpath, DOMUtils::getDOMNodeListIterator($nodes), $cachedCourses);
        }
    }

    public static function fromDOM(DOMDocument $page, CourseCollection $cachedCourses = NULL, String $name = ''): Specialization {
        $specialization = new Specialization($name);
        if (is_null($cachedCourses)) {
            $cachedCourses = new CourseCollection();
        }

        $xpath = new DOMXPath($page);
        $mainBlock = $xpath->query(".//*[@id='block-system-main']")[0];
        $specialization->addCoreCoursesFromDOM($xpath, $mainBlock, $cachedCourses);
        $specialization->addElectiveCoursesFromDOM($xpath, $mainBlock, $cachedCourses);

        return $specialization;
    }
}
