<?php

namespace GATech;

use GATech\Course;
use ArrayAccess;
use Countable;
use IteratorAggregate;
use TypeError;
use ArrayIterator;
use JetBrains\PhpStorm\Pure;
use JsonSerializable;

class CourseCollection implements ArrayAccess, Countable, IteratorAggregate, JsonSerializable {
    private array $courses;
    public int $minimumNeeded = 0;

    public function __construct($array = array()) {
        $this->courses = [];
        foreach($array as $course) {
            $this->courses[$course->code] = $course;
        }
    }

    #[Pure] public function offsetExists($offset): bool {
        if (is_a(Course::class, $offset)) {
            return array_key_exists($offset->code, $this->courses);
        } else {
            return array_key_exists($offset, $this->courses);
        }
    }

    #[Pure] public function offsetGet($offset): Course {
        if (is_a(Course::class, $offset)) {
            return $this->courses[$offset->code];
        } else {
            return $this->courses[$offset];
        }
    }

    public function offsetSet($offset, $value) {
        if (!($value instanceof Course) || !$value->isValid) {
            throw new TypeError('CourseCollection can only hold valid courses');
        } else {
            if ($offset) {
                $this->courses[$offset] = $value;
            } else {
                $this->courses[$value->code] = $value;
            }
        }
    }

    public function offsetUnset($offset) {
        if (is_a(Course::class, $offset)) {
            unset($this->courses[$offset->code]);
        } else {
            unset($this->courses[$offset]);
        }
    }

    #[Pure] public function count(): int {
        return count($this->courses);
    }

    public function getIterator(): ArrayIterator {
        return new ArrayIterator($this->courses);
    }

    public function append(Course $value) {
        $this[$value->code] = $value;
    }

    public function jsonSerialize() {
        if ($this->minimumNeeded > 0) {
            return [
                'courseListing' => $this->courses,
                'minimumNeeded' => $this->minimumNeeded
            ];
        } else {
            return $this->courses;
        }
    }
}