<?php


namespace GATech;

use DOMNode;
use DOMDocument;
use DOMXPath;
use JetBrains\PhpStorm\Pure;
use Seal\Request;
use Seal\DOMUtils;
use DateTime;
use mb_strpos;


class Semester {
    public string $name;
    public int $year;
    public int $season;
    public DateTime $startDate;
    public DateTime $endDate;

    public function __construct(String $name) {
        $this->name = $name;
        $this->season = self::nameToSeason($name);
        $this->year = self::nameToYear($name);
    }

    public function getId(): String {
        return $this->year . '-' . $this->season;
    }

    public static function getCurrentSemester(array $semesters): Semester {
        $today = new DateTime();
        $current = null;
        foreach($semesters as $semester) {
            if ($semester->startDate <= $today) {
                $current = $semesters;
            }
        }
        return $current;
    }

    #[Pure] public static function nameToSeason(String $name): int {
        return match(true) {
            str_contains($name, 'Fall') => 3,
            str_contains($name, 'Summer') => 2,
            str_contains($name, 'Spring') => 1,
            default => 0
        };
    }

    public static function nameToYear(String $name): int {
        preg_match('/\d+/', $name, $matches);
        return intval($matches[0]);
    }

    public static function parseCalendarTerm(String $academicYear, array $cachedSemesters = []): array {
        $csvFile = Request::getText('https://registrar.gatech.edu/feeds/' . $academicYear . '/gt-academic-calendar-subscription-table.csv');

        $lines = explode("\n", $csvFile);
        $headers = str_getcsv(array_shift($lines));
        $semesters = $cachedSemesters;

        foreach($lines as $line) {
            $values = str_getcsv($line);
            if (count($values) === count($headers)) {
                $row = array_combine($headers, $values);
                $name = str_replace('All ', '', $row['Term']);
                $name = str_replace('Full ', '', $name);
                $name = str_replace('Sessions ', '', $name);
                $name = str_replace('Late ', '', $name);
                $name = str_replace('Early ', '', $name);
                $name = str_replace('Short ', '', $name);

                if (!array_key_exists($name, $semesters)) {
                    $semester = new Semester($name);
                    $semesters[$name] = $semester;
                } else {
                    $semester = $semesters[$name];
                }

                preg_match('/\\d{4}\\/\\d{2}\\/\\d{2}/', $row['Date'], $matches);
                if (count($matches) > 1) {
                    $date = new DateTime($matches[1]);

                    if ($row['Title'] === 'First Day of Classes') {
                        $semester->startDate = $date;
                    } else if ($row['Title'] === 'End of Term') {
                        $semester->endDate = $date;
                    }
                }
            }
        }

        return $semesters;
    }

    public static function allFromDOM(DOMDocument $doc): array {
        $xpath = new DOMXPath($doc);
        $academicYears = array_map(
            function(DOMNode $option) {
                return $option->attributes->getNamedItem('value');
            },
            DOMUtils::getDOMNodeListArray($xpath->query("//select[@id='select-collection']/option"))
        );


        $semesters = [];
        foreach($academicYears as $academicYear) {
            $semesters = self::parseCalendarTerm($academicYear->textContent, $semesters);
        }
        return $semesters;
    }
}