<?php

namespace GATech;
use JetBrains\PhpStorm\Pure;
use GATech\CourseDetails;
use Seal\Request;
use DOMNode;

class Course {
    public bool $isFoundational = FALSE;
    public String $name = '';
    public String $formerly = '';
    public String $code = '';
    public String $dept = '';
    public String $number = '';
    public String $link = '';
    public CourseDetails $details;
    public bool $isValid = FALSE;

    #[Pure] public function __construct() {
        $this->details = new CourseDetails();
    }

    public static function fromListItem(DOMNode $listItem): Course {
        $course = new Course();

        $listItemStr = '';
        $linkEl = NULL;
        foreach($listItem->childNodes as $child) {
            if ($child->nodeName !== 'sup') {
                $listItemStr .= $child->textContent;
            }
            if ($child->nodeName === 'a') {
                $linkEl = $child;
            }
        }

        $matches = [];
        preg_match('/(\*)?(([A-Z]+) (\d+( O\d+)?)):? ([^(]+)(\(formerly ([^)]*)\))?/', strval($listItemStr), $matches);

        if ($matches) {
            $course->isValid = TRUE;

            if ($linkEl) {
                $course->link = $linkEl->attributes->getNamedItem('href')->value;
            }

            if ($matches[1] === '*') {
                $course->isFoundational = TRUE;
            }

            $course->code = $matches[2];
            $course->dept = $matches[3];
            $course->number = $matches[4];
            $course->name = trim($matches[6]);

            if (count($matches) > 7) {
                $course->formerly = $matches[8];
            }
        }

        /*if ($course->link) {
            $course->details = CourseDetails::fromDOM(Request::getDOM($course->link));
        }*/

        return $course;
    }

    public function __toString(): String {
        if ($this->isValid) {
            $str = $this->isFoundational ? '*' : '';
            $str .= $this->code;
            $str .= ': ' . $this->name;
            if ($this->formerly) {
                $str .= '(formerly ' . $this->formerly . ')';
            }

            return $str;
        }

        return 'INVALID';
    }
}
