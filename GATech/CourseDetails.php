<?php
namespace GATech;

use DOMDocument;
use DOMXpath;
use DOMNodeList;
use ArrayIterator;
use Seal\DOMUtils;

class CourseDetails {

    public String $overview = '';
    public String $courseGoals = '';
    public array $sampleSyllabus = [];
    public String $videos = '';
    public String $prerequisites = '';

    public static function fromDOM(DOMDocument $doc) {
        $details = new CourseDetails();
        $xpath = new DOMXpath($doc);

        $body = $xpath->query("//div[contains(@class, 'body')]")[0];

        $it = DOMUtils::getDOMNodeListIterator($xpath->query("//div[contains(@class, 'body')]/*"));
        $it->next();

        $currentElement = null;
        while ($it->valid()) {
            if ($it->current()->nodeName === 'h4') {
                $currentElement = match ($it->current()->textContent) {
                    'Overview' => 'overview',
                    'Before Taking This Class...' => 'prerequisites',
                    'Sample Syllabus' => 'sampleSyllabus',
                    'Course Goals' => 'courseGoals',
                    default => null,
                };
            } else if ($currentElement) {
                if ($currentElement === 'sampleSyllabus') {
                    $links = $xpath->query('.//a', $it->current());
                    for ($i = 0; $i < $links->count(); ++$i) {
                        $details->sampleSyllabus[] = $links->item($i)->attributes->getNamedItem('href')->textContent;
                    }
                } else if (in_array($it->current()->nodeName, ['p', 'ul'])) {
                    $details->$currentElement .= $doc->saveHTML($it->current());
                }
            }
            $it->next();
        }

        $videosLinks = $xpath->query(".//a[contains(@href, 'course-videos')]", $body);
        if ($videosLinks && count($videosLinks) > 0) {

            $details->videos = $videosLinks[0]->attributes->getNamedItem('href')->textContent;
        }

        return $details;
    }
}