<?php
spl_autoload_register(function ($class) {
    include_once __DIR__ . DIRECTORY_SEPARATOR . str_replace('\\', DIRECTORY_SEPARATOR, $class) . '.php';
});

use Seal\Request;
use GATech\Semester;

$calendarDOM = Request::getDOM('https://registrar.gatech.edu/calendar');
$semesters = Semester::allFromDOM($calendarDOM);

echo json_encode($semesters);
